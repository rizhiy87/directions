<?php declare(strict_types=1);

class Point
{
    /** @var float */
    private $x;

    /** @var float */
    private $y;

    /**
     * Point constructor.
     * @param float $x
     * @param float $y
     */
    public function __construct(float $x, float $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return float
     */
    public function getX(): float
    {
        return $this->x;
    }

    /**
     * @return float
     */
    public function getY(): float
    {
        return $this->y;
    }

    public function move(float $angle, float $distance): self
    {
        $x = $this->x + $distance * cos(deg2rad($angle));
        $y = $this->y + $distance * sin(deg2rad($angle));
        return new self($x, $y);
    }

    public function getDistanceToPoint(Point $target): float
    {
        $dx2 = pow(($target->getX() - $this->x), 2);
        $dy2 = pow(($target->getY() - $this->y), 2);
        return sqrt($dx2 + $dy2);
    }
}
