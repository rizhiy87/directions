<?php

include '.' . DIRECTORY_SEPARATOR . 'Point.php';
include '.' . DIRECTORY_SEPARATOR . 'WalkCase.php';

const INPUT_FILE = 'input.dat';
const OUTPUT_FILE = 'output.dat';

// Yes it might be a DataLoader class, but KISS
function loadCases(string $fileName): array
{
    $result = [];
    $inputFile = fopen($fileName, 'r');
    while (!feof($inputFile)) {
        $numberOfCases = fgets($inputFile);
        $caseSet = [];
        for ($i = 0; $i < $numberOfCases; $i++) {
            $caseSet[] = trim(fgets($inputFile));
        }
        if (!empty($caseSet)) {
            $result[] = $caseSet;
        }
    }
    fclose($inputFile);

    return $result;
}

// It also can be CaseProcessor class
function processCase(array $input): WalkCase
{
    $startX = array_shift($input);
    $startY = array_shift($input);
    $walkCase = WalkCase::createFromCoordinates($startX, $startY);

    while (!empty($input)) {
        $operation = array_shift($input);
        $argument = array_shift($input);
        switch ($operation) {
            case 'start':
                $walkCase->start($argument);
                break;
            case 'walk':
                $walkCase->walk($argument);
                break;
            case 'turn':
                $walkCase->turn($argument);
                break;
        }
    }
    return $walkCase;
}

/**
 * @param WalkCase[] $walks
 * @return Point
 */
function getAverageCoordinate(array $walks): Point
{
    $sumX = 0;
    $sumY = 0;

    foreach ($walks as $walk) {
        $sumX += $walk->getCoordinates()->getX();
        $sumY += $walk->getCoordinates()->getY();
    }

    return new Point(
        $sumX / count($walks),
        $sumY / count($walks)
    );
}

$cases = loadCases('.' . DIRECTORY_SEPARATOR . INPUT_FILE);

$results = [];
foreach ($cases as $caseSet) {
    /** @var WalkCase[] $walks */
    $walks = [];
    foreach ($caseSet as $case) {
        $walks[] = processCase(explode(' ', $case));
    }

    $averagePoint = getAverageCoordinate($walks);

    $max = 0;
    foreach ($walks as $walk) {
        $distance = $walk->getCoordinates()->getDistanceToPoint($averagePoint);
        $max = max($max, $distance);
    }

    $results[] = round($averagePoint->getX(), 4) . " " . round($averagePoint->getY(), 4) . " " . round($max, 4);
}

// Save results. Yes it might be a separate repository, but this is just a test case
$file = fopen('.' . DIRECTORY_SEPARATOR . OUTPUT_FILE, 'w');
foreach ($results as $result) {
    fputs($file, $result . "\n");
}
fclose($file);