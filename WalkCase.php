<?php declare(strict_types=1);

class WalkCase
{
    /** @var Point */
    private $coordinates;

    /** @var float */
    private $angle;

    /**
     * WalkCase constructor.
     * @param Point $coordinates
     */
    public function __construct(Point $coordinates)
    {
        $this->coordinates = $coordinates;
        $this->angle = 0;
    }

    public static function createFromCoordinates(float $x, float $y): self
    {
        return new self(new Point($x, $y));
    }

    /**
     * @return Point
     */
    public function getCoordinates(): Point
    {
        return $this->coordinates;
    }

    public function start(float $angle): self
    {
        $this->angle = $angle;
        return $this;
    }

    public function turn(float $angle): self
    {
        $this->angle += $angle;
        return $this;
    }

    public function walk(float $distance): self
    {
        $this->coordinates = $this->coordinates->move($this->angle, $distance);
        return $this;
    }
}